/*
 * Improved version of https://www.thingiverse.com/thing:23685
 * // TODO: implement the improvements... if only openjscad had a sane way of doing chamfers.
 * - Added a stand
 * - Added chamfers to corners for better fit
 */

function getParameterDefinitions() {
  return [
    { name: "block_size", type: "int", initial: 5, caption: "Block size (mm)" },
    {
      name: "part_offset",
      type: "int",
      initial: 2,
      caption: "Space between parts (for plating)"
    }
  ];
}

const main = parameters => {
  const bs = parameters.block_size;

  // TODO: refactor into array and loop or something to create offsets?
  const pieceA = union(
    cube({ size: [bs, 3 * bs, bs] }),
    cube({ size: [bs, bs, 2 * bs] }).translate([bs, bs, 0])
  );
  const pieceB = union(
    cube({ size: [bs, 2 * bs, bs] }),
    cube({ size: [bs, 2 * bs, bs] }).translate([bs, bs, 0]),
    cube({ size: [bs, bs, bs] }).translate([bs, bs, bs])
  ).translate([pieceA.getBounds()[1].x + parameters.part_offset, 0, 0]);
  const pieceC = union(
    cube({ size: [bs, bs, bs] }).translate([0, bs, 0]),
    cube({ size: [bs, 3 * bs, bs] }).translate([bs, 0, 0]),
    cube({ size: [bs, bs, bs] }).translate([bs, 2 * bs, bs])
  ).translate([pieceB.getBounds()[1].x + parameters.part_offset, 0, 0]);
  const pieceD = union(
    cube({ size: [bs, 2 * bs, bs] }),
    cube({ size: [bs, bs, 2 * bs] }).translate([bs, bs, 0])
  ).translate([pieceC.getBounds()[1].x + parameters.part_offset, 0, 0]);
  const pieceE = union(
    cube({ size: [bs, bs, bs] }).translate([0, bs, 0]),
    cube({ size: [bs, 3 * bs, bs] }).translate([bs, 0, 0])
  ).translate([pieceD.getBounds()[1].x + parameters.part_offset, 0, 0]);
  const pieceF = union(
    cube({ size: [bs, bs, bs] }).translate([0, 2 * bs, 0]),
    cube({ size: [bs, 3 * bs, bs] }).translate([bs, 0, 0])
  ).translate([pieceE.getBounds()[1].x + parameters.part_offset, 0, 0]);

  return [pieceA, pieceB, pieceC, pieceD, pieceE, pieceF];
};
