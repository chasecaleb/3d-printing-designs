#!/bin/bash

export_stl() {
    SRC=$1
    DEST_DIR=./build
    DEST=$DEST_DIR/$(basename "$(dirname "$SRC")").stl

    if [[ ! -e "$DEST_DIR" ]]; then
       mkdir "$DEST_DIR"
    fi

    echo "Exporting $SRC to $DEST"
    npx openjscad "$SRC" -o "$DEST"
}
# Make available via subshell for find
export -f export_stl

if [[ $# == 0 ]]; then
    echo "No arguments passed... converting all files"
    find src -type f -name index.jscad -exec bash -c 'export_stl "$0"' {} \;
elif [[ $# == 1 ]]; then
    export_stl "$1"
else
    echo "Unknown arguments"
    exit 1
fi
