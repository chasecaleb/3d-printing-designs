# 3D Printing Designs
> Modeled with [OpenJSCAD](https://openjscad.org)

* To view/edit source files: drag-and-drop file into the [OpenJSCAD editor](https://openjscad.org)
* You can also use the [cli](https://github.com/jscad/OpenJSCAD.org/tree/master/packages/cli)
* OpenJSCAD documentation is [available here](https://openjscad.org/dokuwiki/)
